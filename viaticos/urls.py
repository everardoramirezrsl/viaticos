from django.conf.urls import url

from . import views

urlpatterns = [url(r'^$', views.inicio, name='inicio'),
               url(r'^registro_oficio_de_comision/$', views.registro_oficio_de_comision, name='registroComision'),
               url(r'detalle_oficio_de_comision/(?P<oficio_id>[0-9]+)/$', views.detalle_oficio_de_comision,
                   name='detalleComision'),
               url(r'consulta_ofcicio_de_comision/$', views.consulta_oficio_de_comision, name='consultaComision')]
