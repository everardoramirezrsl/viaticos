from django.contrib.auth.models import User
from django.db import models


# # Create your models here.
# class ViaticoLocal(models.Model):
#     funcionario = models.OneToOneField(User, on_delete=models.CASCADE)
#     fecha_inicio = models.DateField()
#     fecha_final = models.DateField()
#     destino = models.TextField()
#     tipo_transporte = models.TextField()
#     salida_transporte = models.DateTimeField()
#     regreso_transporte = models.DateTimeField()
#
#     class Meta:
#         verbose_name_plural = "viaticos locales"
#
#
# class ViaticoNacional(models.Model):
#     funcionario = models.OneToOneField(User, on_delete=models.CASCADE)
#     fecha_inicio = models.DateField()
#     fecha_final = models.DateField()
#     destino = models.TextField()
#     tipo_transporte = models.TextField()
#     salida_transporte = models.DateTimeField()
#     regreso_transporte = models.DateTimeField()
#
#     class Meta:
#         verbose_name_plural = "viaticos locales"
#
#
# class ViaticoExtranjero(models.Model):
#     funcionario = models.OneToOneField(User, on_delete=models.CASCADE)
#     fecha_inicio = models.DateField()
#     fecha_final = models.DateField()
#     destino = models.TextField()
#     eventos = models.TextField()
#     agenda = models.TextField()
#     objeto_del_viaje = models.TextField()
#     resultado_esperado = models.TextField()
#
#     class Meta:
#         verbose_name_plural = "viaticos extranjero"

class OficioDeComision(models.Model):
    TIPOS_DE_COMISION = (("L", "Local"), ("N", "Nacional"), ("I", "Internacional"))
    comisionado = models.ForeignKey(User, on_delete=models.PROTECT, related_name='+')
    poblacion = models.TextField()
    fecha_inicio = models.DateField()
    fecha_final = models.DateField()
    motivo = models.TextField()
    hora_salida = models.TimeField()
    hora_regreso = models.TimeField()
    medio_transporte = models.TextField()
    tipo_de_comision = models.CharField(max_length=1, choices=TIPOS_DE_COMISION, default="L")
    clave_presupuestal = models.CharField(max_length=100)
    autoridad = models.ForeignKey(User, on_delete=models.PROTECT, related_name='+')

    class Meta:
        verbose_name_plural = "Oficios de comision"


class Campus(models.Model):
    campus = models.CharField(max_length=40)

    class Meta:
        verbose_name_plural = "Campus"


class Area(models.Model):
    area = models.CharField(max_length=30)


class Perfil(models.Model):
    """Permite relacionar a un usuario con los tipos de usuario espesifico """
    AUTORIZACIONES = (("C", "comisionado"), ("D", "Director"))
    autorizacion = models.CharField(max_length=1, choices=AUTORIZACIONES, default="C")
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    area = models.ForeignKey(Area, on_delete=models.CASCADE)
    campus = models.ForeignKey(Campus, on_delete=models.CASCADE)
    puesto = models.CharField(max_length=40)

    def __str__(self):
        return self.user.__str__()

    class Meta:
        verbose_name_plural = "Perfiles"  # nombre largo en plurar arreglado
