from django.contrib import admin
from viaticos.models import OficioDeComision, Campus, Area, Perfil

admin.site.register(OficioDeComision)
admin.site.register(Campus)
admin.site.register(Area)
admin.site.register(Perfil)
