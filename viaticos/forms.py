from django import forms

from viaticos.models import OficioDeComision


class OficioDeComisionForm(forms.ModelForm):
    class Meta:
        model = OficioDeComision
        fields = ["comisionado", "poblacion", "fecha_inicio", "fecha_final", "motivo", "hora_salida", "hora_regreso",
                  "medio_transporte", "tipo_de_comision", "clave_presupuestal", "autoridad"]
        widgets = {
            "fecha_inicio": forms.TextInput(attrs={'type': 'date'}),
            "fecha_final": forms.TextInput(attrs={'type': 'date'}),
            "hora_salida": forms.TextInput(attrs={'type': 'time'}),
            "hora_regreso": forms.TextInput(attrs={'type': 'time'})
        }


class RangoFechasForm(forms.Form):
    fecha_inicio = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'}))
    fecha_fin = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'}))
