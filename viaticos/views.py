from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render, get_object_or_404

# Create your views here.
from viaticos.forms import OficioDeComisionForm, RangoFechasForm
from viaticos.models import Perfil, OficioDeComision


@login_required
def registro_oficio_de_comision(request):
    """Vista para el proceso de registro de una comisión"""
    titulo = "Registro oficio de comision"
    form = OficioDeComisionForm(request.POST or None)
    # form.fields["comisionado"].queryset = request.user#User.objects.all()
    # form.fields["autoridad"].queryset = request.user
    context = {"titulo": titulo,
               "el_form": form}
    if form.is_valid():
        comision = form.save()

    return render(request, "formulario.html", context)


@login_required
def consulta_oficio_de_comision(request):
    """muesra un listado de los oficios de comision"""
    titulo = "Consulta oficios"
    form = RangoFechasForm(request.GET or None)
    titulos = ["Comisionado", "fecha de inicio"]
    context = {"titulo": titulo, "el_form": form, "titulos": titulos}
    if form.is_valid():
        form_data = form.cleaned_data
        consulta = OficioDeComision.objects.filter(
            fecha_inicio__range=(form_data.get("fecha_inicio"), form_data.get("fecha_fin")))
        context = {"titulo": titulo, "el_form": form, "titulos": titulos, "consulta": consulta}
    return render(request, "consulta/consultaOficioDeComision.html", context)


@login_required
def detalle_oficio_de_comision(request, oficio_id):
    """musra informacion relativa a las comisones"""
    oficio = get_object_or_404(OficioDeComision, pk=oficio_id)
    context = {"oficio": oficio}
    return render(request, "consulta/detalleOficioDeComision.html",context)


@login_required
def inicio(request):
    """la vista mas bassica solo solo muestra la bienvenida"""
    titulo = "Bienvenido %s" % request.user
    context = {"titulo": titulo}
    return render(request, "inicio.html", context)
