from django.apps import AppConfig


class ViaticosConfig(AppConfig):
    name = 'viaticos'
